from django.shortcuts import render
from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from rest_framework.authtoken.models import Token
import re
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from home.models import Imagelist
from home.zillow_ui import zillow_get
from django.contrib import messages
import json
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def index(request):
    response = {
        'message': 'Welcome to Scrapping Hero API'
    }
    return Response(response)


def home(request):
    return render(request, 'homepage.html')


@login_required(login_url='/login/')
def roompage(request):
    return render(request, 'room.html')


def craeteusername(email):
    username = email.split("@")[0]
    username = re.sub('[^A-Za-z0-9\.]+', '', username)
    username = username.lower()
    orignam_uname = username
    exists = True
    count = 1
    while exists:
        exists = User.objects.filter(username=username).exists()
        if exists:
            if count < 10:
                username = orignam_uname + "0" + str(count)

            else:
                username = orignam_uname + str(count)
            count += 1
    return username.lower()


def Token_register(request):
    try:
        lastname = ''
        token_key = None
        msg = None
        if request.method == "POST":
            exists_check = request.POST.get('txt_email')

            user_msg = User.objects.filter(email=exists_check).count()
            if user_msg > 0:
                msg = 'Email Already Exists'
            else:
                usr_password = request.POST.get('txt_password')
                objU = User()
                objU.first_name = request.POST.get('txt_fname')
                objU.last_name = lastname
                username = craeteusername(request.POST.get(
                    'txt_email'))  # create username from email
                objU.username = username
                objU.email = request.POST.get('txt_email')
                objU.password = usr_password
                objU.set_password(objU.password)
                objU.is_active = True
                objU.is_superuser = False
                objU.is_staff = False
                objU.save()
                token = Token.objects.create(user=objU)
                token_key = token.key
        return render(request, 'Token_register.html', {"token": token_key, "msg": msg})

    except:
        import sys
        return HttpResponse(str(sys.exc_info()))


def login1(request):
    msg = None
    if request.method == "POST":
        username = request.POST.get('txt_username')
        password = request.POST.get('txt_password')
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):

                objuser = user
            else:
                objuser = None

        except:
            objuser = None

        if objuser is not None:
            if objuser.is_active:
                login(request, objuser)
                token = Token.objects.get_or_create(user=objuser)
                token = token[0]
                token_key = token.key
                msg = token_key
            return render(request, 'room.html', {"msg1": msg})
            # return HttpResponseRedirect('/room/')
        else:
            msg = 'Wrong Password'
    return render(request, 'login.html', {"msg1": msg})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login/')

import subprocess

def checkRunningProcess(filepath):
        '''
        Method to check running file process
        Args:
            filepath : path of file
        '''
        filename = str(filepath).split('/')[-1]
        prcss = subprocess.Popen("ps -eaf | grep "+str(filename), stdout=subprocess.PIPE,
                                 shell=True)
        (output, err) = prcss.communicate()
        output = output.decode("utf-8")
        output = output.split()
        process_count = output.count(filepath)
        print('Counttt',output)
        if process_count >= 1:
            return True
        else:
            return False

@login_required(login_url='/login/')
def ProcessCsvFiles(request):
    
    print('Hreee')
    data = {}
    print(request.method)
    if "GET" == request.method:
        return render(request, "upload_csv.html", data)
    # if not GET, then proceed with processing
    try:

        csv_file = request.FILES["csv_file"]
        print(csv_file)
        if not csv_file.name.endswith('.csv'):
            messages.error(request,'File is not CSV type')
            return HttpResponse('Csv Error')
    #if file is too large, return message
        if csv_file.multiple_chunks():
            messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return HttpResponse('Csv Error')
        
        
        try:
            file_data = csv_file.read().decode("utf-8")
            lines = file_data.split(" ")
            for line in lines:
                print(line)
                import sys
                import os
                
                # import pkill
                filename='zillow_ui'
                filename = filename.replace(' ','_').strip()
                csv_file = os.path.join(os.path.dirname(__file__),filename+".py")
                print(csv_file)
                print(os.getpid())
                import os, signal
                
                # from check_run import checkRunningProcess
                check = checkRunningProcess(csv_file)
                print(check)
                if check==True:
                    status='stop'
                    # pkill csv_file
                else:
                    status='start'
                    os.kill(os.getpid(), signal.SIGKILL)
                # data=json.dumps(str(zillow_get(line,status)))
                # print(sys.exc_info())
                # print(type(data))
                # print(data)
            return HttpResponse('data')
            
        except:
            return HttpResponse('Error')

    except:
        return HttpResponse('Error')


